#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <iostream>
#include <thread>
#include <vector>
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

void initWinsock(struct addrinfo& hints, WSADATA& wsaData);
int acceptClient(SOCKET cSocket);
int listenSocket(std::vector<std::thread>& tArray);

int main(){
	std::vector<std::thread> tArray;
	listenSocket(tArray);
	for (size_t i = 0; i < tArray.size(); i++)
		tArray[i].join();
	return 0;
}

int listenSocket(std::vector<std::thread>& tArray)
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	initWinsock(hints, wsaData);

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	while (ClientSocket = accept(ListenSocket, NULL, NULL)){
		if (ClientSocket == INVALID_SOCKET) {
			std::cout << "accept failed with error:" << WSAGetLastError() << std::endl;
			closesocket(ListenSocket);
			WSACleanup();
			return 1;
		}

		tArray.push_back(std::thread(acceptClient, ClientSocket));
	}
	closesocket(ListenSocket);

	WSACleanup();
	return 0;
}


int acceptClient(SOCKET cSocket){
	if (send(cSocket, "Accepted.", 10, 0) == SOCKET_ERROR) {
		std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
		closesocket(cSocket);
		WSACleanup();
		return 0;
	}
	if (shutdown(cSocket, SD_SEND)) {
		std::cout << "shutdown failed with error: " <<  WSAGetLastError() << std::endl;
		closesocket(cSocket);
		return 0;
	}

	// cleanup
	closesocket(cSocket);
	return 1;
}

void initWinsock(struct addrinfo& hints, WSADATA& wsaData){
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
	}
	else{
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;
	}
}
