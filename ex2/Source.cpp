#include <iostream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>


bool empty = true;
int read_counter = 0;
std::mutex mtx;
std::condition_variable cv;
bool exitAll = true;
/*
Because the exercise does not require us to actually read / write, I chose to demonstrate it by:
Prints . to demonstrate reading
Prints , to demonstrate writing
*/

void readFu(){
	std::unique_lock<std::mutex> ulock(mtx);
	read_counter++;
	while (empty && exitAll){
		cv.wait(ulock);
	}
	if (exitAll){
		std::cout << "Reading from file";
		for (int i = 0; i < 50; i++)
			std::cout << '.';
		std::cout << std::endl;
		empty = true;
		--read_counter;
		cv.notify_one();
	}
}

void writeFu(){
	std::unique_lock<std::mutex> ulock(mtx);
	while (!empty && read_counter > 0){ // prioritizing the readers.
		cv.wait(ulock);
	}
	std::cout << "Writing to file";
	for (int i = 0; i < 50; i++)
		std::cout << ",";
	std::cout << std::endl;
	empty = false;
	cv.notify_one();
}

void closeAll(){
	exitAll = false;
	cv.notify_all();
}

int main(){
	int option = -1;
	std::vector<std::thread> ths;
	std::cout << "Choose an option:\n1 - Write\n2 - Read\n3 - Close" << std::endl;
	do{
		std::cin >> option;
		switch (option){
		case 1: ths.push_back(std::thread(writeFu));
			break;
		case 2: ths.push_back(std::thread(readFu));
			break;
		}
	} while (option != 3);
	closeAll();
	for (unsigned int i = 0; i < ths.size(); i++)
		ths[i].join();
	return 0;
}