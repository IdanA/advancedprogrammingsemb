#include "buffer.h"

using namespace std;

bool Buffer::push(Message &msg){
	lock_guard<std::mutex> GuardiaN(message_mutex);
	int size = message_queue.size();
	message_queue.push(msg);
	if (message_queue.size() > size)
		return true;
	else
		return false;
}

Message Buffer::pop(){
	lock_guard<mutex> GuardiaN(message_mutex);
	return message_queue.pop();
}