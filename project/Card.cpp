#include "Card.h"

Card::Card(CARDS _type, COLORS _color)
: type(_type)
, color(_color)
{}

Card::Card()
{}

std::string Card::to_string() const
{
	string cardS;
	cardS.push_back(type);
	cardS.push_back(color);
	return cardS;
}

CARDS Card::getType() const
{
	return type;
}

COLORS Card::getColor() const
{
	return color;
}

bool Card::legalCard(Card other) const
{
	return color == other.color || type == other.type;
}
