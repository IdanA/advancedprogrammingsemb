#include "User.h"

User::User(string name, bool admin, SOCKET uSocket)
: user_name(name)
, is_admin(admin)
, room(nullptr)
, user_socket(uSocket)
{}

std::string User::getUName() const
{
	return user_name;
}

Room* User::getRoom() const
{
	return room;
}

void User::setRoom(Room* _room)
{
	room = _room;
}

bool User::isAdmin() const
{
	return is_admin;
}

SOCKET User::getSocket() const
{
	return user_socket;
}

void User::setSocket(SOCKET s)
{
	user_socket = s;
}
