#pragma once
#include <stdio.h>
#include <string>
#include <queue>
#include <mutex>

using namespace std;


class Buffer{
private:
	queue<Message> message_queue;
	mutex message_mutex;

public:
	bool push(Message &msg);
	Message pop();
	bool is_empty();
};