#pragma once

#include <string>
#include <vector>
#include <ctype.h>
#include "Card.h"
#include "Room.h"
class StringHandler
{
public:
	static std::vector<std::string> stringSplit(std::string str, char delim);
	static std::string extractData(std::string message);
	static bool StringHandler::checkMsg(std::string msg);
	static bool isNum(std::string str);
	static std::string formatString(std::vector<Card> vec);
	static std::string formatString(std::vector<Room> vec);
};