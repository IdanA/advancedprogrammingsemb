#pragma once;
#include <winsock2.h>
#include <iostream>
#include <map>
#include "User.h"
#include <vector>
#include "Room.h"
#include "sqlite3.h"
#include <map>
#include <ctime>
using namespace std;

class Manager{
private:
	map<string, User*> loginUsers;
	vector<Room> room_vector;
	sqlite3* db;

public:
	Manager();
	~Manager();
	bool register_user(User const &user,string password);
	bool login_user(User const &user, string password);
	bool is_exist(User const &user);
	User* try_login(string user_name, string user_password);
	bool add_room(User const &admin, string room_name);
	bool remove_room(Room const &room);
	bool start_game_db(Room const &room);
	bool end_game_db(Room const &room);
	void client_requests_thread(SOCKET sock);
	vector<Room> retRooms();
};

#pragma warning(disable:4996)