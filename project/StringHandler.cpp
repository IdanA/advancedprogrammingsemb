#include <sstream>
#include "StringHandler.h"

//************************************
// Method:    stringSplit
// Returns:   std::vector<std::string>
// Description: Splits a strong according to a given token.
//				Creates a vector with the resulting strings.
// Parameter: std::string str
// Parameter: char delim
//************************************
std::vector<std::string> StringHandler::stringSplit(std::string str, char delim)
{
	std::istringstream stream(str);
	std::string curr;
	std::vector<std::string> elems;

	while (getline(stream, curr, delim))
		elems.push_back(curr);
	return elems;
}



std::string StringHandler::extractData(std::string message)
{
	return message.substr(1, message.find("||") - 1);
}

bool StringHandler::checkMsg(std::string msg)
{ 
	int len = msg.length();
	if (msg[0] != '@')
		return false;

	if (msg.substr(len - 2, len - 1) != "||")
		return false;

	return true;
}

bool StringHandler::isNum(std::string str)
{
	for (size_t i = 0; i < str.length(); i++)
	{
		if (!isdigit(str[0]))
			return false;
	}
	return true;
}


std::string StringHandler::formatString(std::vector<Card> vec)
{
	std::string str = "|";
	for (size_t i = 0; i < vec.size(); i++)
	{
		str += vec[i].to_string();
		str += '|';
	}
	return str + '|';
}


std::string StringHandler::formatString(std::vector<Room> vec)
{
	std::string str = "|";
	for (size_t i = 0; i < vec.size(); i++)
	{
		str += vec[i].getname();
		str += '|';
	}
	return str + '|';
}