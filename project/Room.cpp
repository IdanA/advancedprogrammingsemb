#include <random>
#include <algorithm>
#include "Room.h"
#include "StringHandler.h"

bool Room::add_user(User& user)
{
	if (players_cnt == 4)
	{
		send(user.getSocket(), "@123||", 1024, NULL);
		return false;
	}
	players[players_cnt - 1] = &user;
	players_cnt++;
	return true;
}

Room::Room(string rn, User* a)
: room_name(rn)
, admin(a)
, players_cnt(1)
, draw_counter(0)
, turn_modifier(1)
, current_player(0)
{
	COLORS colors[] = { RED, BLUE, YELLOW, GREEN };
	CARDS types[] = { ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, PLUS, PLUS_TWO, TAKI, CHANGE_DIR, STOP };
	for (int i = 0; i < 14; i++)
	{
		for (int j = 0; j < 4; j++)
			deck.push_back(Card(types[i], colors[j]));
	}
	deck.push_back(Card(SUPER_TAKI, GREEN));
	deck.push_back(Card(CHANGE_COLOR, GREEN));
	random_shuffle(deck.begin(), deck.end());
}

void Room::delete_user(User& user)
{
	for (size_t i = 0; i < 4; i++)
	{
		if (players[i] == &user)
		{
			players.erase(players.begin() + i);
		}
	}
	players_cnt--;
}

bool Room::is_open() const
{
	return in_game;
}

void Room::close()
{
	for (int i = 0; i < players_cnt; i++)
		players.pop_back();
	delete admin;
	in_game = false;
}

bool Room::is_in_room(User const &user) const
{
	for (size_t i = 0; i < 4; i++)
	{
		if (players[i] == &user)
			return true;
	}
	return false;
}

bool Room::start_game()
{
	if (in_game)
		return false;

	for (int i = 0; i < players_cnt; i++)
		send(players[i]->getSocket(), "@112||", 1024, NULL);
	return (in_game = true);
}

std::string Room::getname() const
{
	return room_name;
}

bool Room::is_draw_legal(int num_of_cards)
{
	if (num_of_cards != draw_counter)
	{
		send(players[current_player]->getSocket(), "@223||", 1024, NULL);
		return false;
	}
	return true;
}

bool Room::is_turn_legal(vector<Card> moves)
{
	size_t i, cardsLeft;
	if (last_card.getType() == PLUS_TWO && (moves[0].getType() != PLUS_TWO || moves.size() != 1))
	{
		send(players[current_player]->getSocket(), ("@221|" + moves[0].to_string() + "||").c_str(), 1024, NULL);
		return false;
	}
	for (i = 0, cardsLeft = 1; i < moves.size() && cardsLeft; i++)
	{
		switch (moves[i].getType())
		{
		case PLUS:
			cardsLeft++;
			break;
		case TAKI:
		case SUPER_TAKI:
			cardsLeft = -10; // no card limit
			break;
		}

		if (moves[i].getType() != CHANGE_COLOR && moves[i].getType() != last_card.getType() && moves[i].getColor() != last_card.getColor())
		{
			string errMsg = "@220|" + moves[i].to_string() + "||";
			send(players[current_player]->getSocket(), errMsg.c_str(), 1024, NULL);
			return false;
		}
	}
	return moves.size() == i;
}

bool Room::play_turn(const vector<Card>& moves)
{
	if (!is_turn_legal(moves))
	{
		send(players[current_player]->getSocket(), "@222||", 1024, NULL);
		return false;
	}
	vector<Card> cardsPlayed;
	if (last_card.getType() == PLUS_TWO)
		draw_counter = 2;
	else
		draw_counter = 0;

	for (size_t i = 0; i < moves.size(); i++)
	{
		cardsPlayed.push_back(moves[i]);
		last_card = moves[i];
	}

	switch (last_card.getType())
	{
	case STOP:
		turn_modifier *= 2;
		break;
	case CHANGE_DIR:
		turn_modifier = -turn_modifier;
		break;
	}
	endTurn(cardsPlayed);
	return true;
}

vector<Card> Room::shuffle_cards(int num_of_cards)
{
	vector<Card> shuffled;

	if (is_draw_legal(num_of_cards))
	{
		for (int i = 0; i < num_of_cards; i++)
		{
			vector<Card>::reverse_iterator last = deck.rbegin();
			shuffled.push_back(*last);
		}
		string msg = "@211|" + shuffled.size();

		for (int i = 0; i < players_cnt; i++)
			send(players[current_player]->getSocket(), (msg + "||").c_str(), 1024, NULL);
	}
	return shuffled;
}

void Room::endTurn(vector<Card> cardsPlayed)
{
	for (int i = 0; i < players_cnt; i++)
	{
		send(players[i]->getSocket(), ("@210" + StringHandler::formatString(cardsPlayed)).c_str(), 1024, NULL);
		send(players[i]->getSocket(), ("@200|" + players[players_cnt + turn_modifier]->getUName() + "||").c_str(), 1024, NULL);
	}
}
/*
vector<vector<Card> > Room::shuffle_cards_start_game(int num_of_players)
{

}
*/


std::string Room::getadmin() const
{
	return admin->getUName();
}	


bool Room::isuserin(User* u)
{
	for (size_t i = 0; i < 4; i++)
	{
		if (players[i]->getUName() == u->getUName())
			return true;
	}
	return false;
}