#pragma once

#include <string>
#include <WinSock2.h>
#include "Room.h"

using namespace std;

class Room;

class User
{
private:
	string user_name;
	Room *room;
	bool is_admin;
	SOCKET user_socket;

public:
	User(string name, bool admin, SOCKET uSocket);
	string getUName() const;
	Room* getRoom() const;
	SOCKET getSocket() const;
	void setRoom(Room* _room);
	void setSocket(SOCKET s);
	bool isAdmin() const;
};