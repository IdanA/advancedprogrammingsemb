#include <iostream>
#include <thread>
#include <map>
#include "gofman.h"
#include "protocol.h"
#include "StringHandler.h"
#include "conn.h"
#include "sqlite3.h"
#include "Manager.h"

using namespace std;

static sqlite3* db;
static Manager manager;
static vector<std::thread> clients;	

void leaveGame(vector<Room> room_list, User* u)
{
	for (size_t i = 0; i < room_list.size(); i++)
	{
		if (room_list[i].isuserin(u))
		{
			room_list[i].delete_user(*u);
			return;
		}
	}
}

void startGame(vector<Room> room_list, User* u)
{
	for (size_t i = 0; i < room_list.size(); i++){
		if (room_list[i].getadmin() == u->getUName()){
			room_list[i].start_game();
			manager.start_game_db(room_list[i]);
		}
	}
}

void joinGame(vector<Room> room_list, string admin, User* u)
{
	Room* curRoom = nullptr;
	for (size_t i = 0; i < room_list.size() && curRoom == nullptr; i++){
		if (room_list[i].getadmin() == admin)
			curRoom = &room_list[i];
	}
	if (curRoom != nullptr)
		curRoom->add_user(*u);
}

void closeGame(vector<Room> room_list, User* u)
{
	for (size_t i = 0; i < room_list.size(); i++){
		if (room_list[i].getadmin() == u->getUName()){
			room_list[i].close();
			manager.end_game_db(room_list[i]);
			return;
		}
	}
}

bool switchRequest(SOCKET clientSoc, int msgID, vector<string> request, User* u)
{
	vector<Room> room_list = manager.retRooms();

	switch (msgID)
	{
	case EN_REGISTER:
		manager.register_user(User(request[1], false, clientSoc), request[2]);
		break;
	case EN_LOGIN:
		u = manager.try_login(request[1], request[2]);
		u->setSocket(clientSoc);
		break;
	case EN_LOGOUT:
		return false;
	case RM_ROOM_LIST:
		break;
	case RM_CREATE_GAME:
		manager.add_room(*u, request[1]);
		break;
	case RM_JOIN_GAME:
		joinGame(room_list, request[1], u);
		break;
	case RM_START_GAME:
		startGame(room_list, u);
		break;
	case RM_LEAVE_GAME:
		leaveGame(room_list, u);
		break;
	case RM_CLOSE_GAME:
		closeGame(room_list, u);
		break;
	}
	return true;
}

bool answerRequest(vector<string> request, SOCKET clientSoc, User* u)
{
	if (!StringHandler::isNum(request[0]))
	{
		send(clientSoc, "@130||", BUFLEN, NULL);
		return true;
	}
	int msgID = stoi(request[0], nullptr);

	return switchRequest(clientSoc, msgID, request, u);
}

int handleClient(SOCKET clientSoc)
{
	int size_recv;
	char buffer[BUFLEN] = "";
	bool IsAlive = true;
	User* u = nullptr;

	while (IsAlive && (size_recv = recv(clientSoc, buffer, BUFLEN, NULL)) != SOCKET_ERROR && size_recv > 0)
	{
		if (StringHandler::checkMsg(buffer))
		{
			vector<string> args = StringHandler::stringSplit(StringHandler::extractData(buffer), '|');
			IsAlive = answerRequest(args, clientSoc, u);
		}
		else send(clientSoc, "@130||", BUFLEN, NULL);
	}
	closeconn(clientSoc);
	return 0;
}
/*
void initialize()
{
	int rc = sqlite3_open("server.db", &db);

	services[EN_REGISTER] = &Manager::register_user;
		services[EN_LOGIN] =
		services[EN_LOGOUT] =
	services[RM_ROOM_LIST] = ;
	services[RM_CREATE_GAME] = 
	services[RM_JOIN_GAME] = 
	services[RM_START_GAME] = 
	services[RM_LEAVE_GAME] = 
	services[RM_CLOSE_GAME] = 
	services[GM_PLAY] = 
	services[GM_DRAW] =
}
*/
int main()
{
	char ip[20] = "";
	SOCKET clientSoc;
	//initialize();
	WSADATA info;
	WSAStartup(MAKEWORD(2, 0), &info);
	VALID_RET(createSocket(), "", -1);
	while (true)
	{
		connectAndListen(&clientSoc, ip);
		clients.push_back(std::thread(handleClient, clientSoc));
	}
	closesocket(clientSoc);
	WSACleanup();
	return 0;
}