#include "Manager.h"
#include "StringHandler.h"
#include "Room.h"

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	int i;
	for (i = 0; i < argc; i++){
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}


static int callback2(void *data, int argc, char **argv, char **azColName){
	bool *tmp = (bool*)data;
	*tmp = (argc > 0);
	return 0;
}

Manager::Manager()
{
	sqlite3_open("server.db", &db);
}

Manager::~Manager()
{
	sqlite3_close(db);
}

bool Manager::register_user(User const &user, string password)
{
	char *zErrMsg = 0;
	int rc;
	SOCKET s = user.getSocket();
	char* uname = (char*)user.getUName().c_str();
	string sql = "INSERT INTO user (username, password_hash) "  \
		"VALUES ( '" + user.getUName() + "', '" + password + "' ); ";
	rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		send(user.getSocket(), "@101||", 1024, NULL);

		return false;
	}
	else{
		string msg = "@122" + StringHandler::formatString(room_vector);
		send(user.getSocket(), msg.c_str(), 1024, NULL);
	}
	sqlite3_close(db);
	return true;
}

bool Manager::login_user(User const &user, string password)
{
	bool tmp;
	string sql = "SELECT * FROM user WHERE username = " + user.getUName() + "and password_hash = " + password + ";";
	char* data = "0";
	char *zErrMsg;
	int rc = sqlite3_exec(db, sql.c_str(), callback2, &tmp, &zErrMsg);
	if (tmp)
	{
		string msg = "@100" + StringHandler::formatString(room_vector);
		send(user.getSocket(), msg.c_str(), 1024, NULL);
	}
	else
		send(user.getSocket(), "@120||", 1024, NULL);
	return tmp;
}

bool Manager::is_exist(User const &user)
{
	string sql = "SELECT * FROM user WHERE username = " + user.getUName() + ";";
	char* data = "0";
	bool tmp;
	char *zErrMsg;
	int rc = sqlite3_exec(db, sql.c_str(), callback2, &tmp, &zErrMsg);
	return tmp;
}

User* Manager::try_login(string user_name, string user_password)
{
	string sql = "SELECT * FROM user WHERE username = " + user_name + "and password_hash = " + user_password + ";";
	bool tmp;
	char *zErrMsg;
	int rc = sqlite3_exec(db, sql.c_str(), callback2, &tmp, &zErrMsg);
	if (tmp){
		User* tmp = new User(user_name, false, 0);
		map<string,User*>::iterator it = loginUsers.begin();
		loginUsers.insert(it, pair<string, User*>(user_name, tmp));
		return	tmp;
	}
	return nullptr;
}

bool Manager::add_room(User const &admin, string room_name)
{
	User tmp = admin;
	room_vector.push_back(Room(room_name, &tmp));
	send(admin.getSocket(), "@102||", 1024, NULL);
	return true;
}

bool Manager::remove_room(Room const &room)
{
	int place = -1;
	for (size_t i = 0; i < room_vector.size(); i++){
		if (room.getname() == room_vector[i].getname())
			place = i;
	}
	if (place == -1)
		return false;
	room_vector.erase(room_vector.begin() + place);
	return true;


}

bool Manager::start_game_db(Room const &room)
{
	std::time_t result = std::time(0);
	string sql = "INSERT INTO game (game_start) "  \
		"VALUES ( " + std::to_string(result) + " ); ";
	char* zErrMsg;
	int rc = sqlite3_exec(db, sql.c_str(), nullptr, 0, &zErrMsg);
	if (rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}
	return true;
}

bool Manager::end_game_db(Room const &room)
{
	int place = -1;
	for (size_t i = 0; i < room_vector.size(); i++){
		if (room.getname() == room_vector[i].getname())
			place = i;
	}
	std::time_t result = std::time(nullptr);
	string sql = "UPDATE game SET game_end = " + std::to_string(result) + " WHERE game_id = " + to_string(1) + ";";
	char* zErrMsg;
	int rc = sqlite3_exec(db, sql.c_str(), nullptr, 0, &zErrMsg);
	if (rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return false;
	}
	return true;
}

vector<Room> Manager::retRooms(){
	return room_vector;
}


