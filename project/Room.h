#pragma once;

#include <string>
#include <vector>
#include <array>
#include "User.h"
#include "Card.h"
#include "protocol.h"

using namespace std;

class User;

class Room
{
private:
	string room_name;
	User* admin;
	vector<User*> players;
	bool in_game;
	int current_player;
	int players_cnt;
	int turn_modifier;
	int draw_counter;
	Card last_card;
	vector<Card> deck;
	
	void endTurn(vector<Card> cardsPlayed);

public:
	Room(string rn, User* a);
	string getname() const;
	string getadmin() const;
	bool add_user(User& user);
	void delete_user(User& user);
	bool is_open() const;
	void close();
	bool is_in_room(User const &user) const;
	bool start_game();
	bool is_turn_legal(vector<Card> moves);
	bool is_draw_legal(int num_of_cards);
	bool play_turn(const vector<Card>& moves);
	vector<Card> shuffle_cards(int num_of_cards);
	vector<vector<Card> > shuffle_cards_start_game(int num_of_players);
	bool isuserin(User* u);
};