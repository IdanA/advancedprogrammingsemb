#pragma once

#include <string>
#include "protocol.h"

using namespace std;

class Card
{
private:
	CARDS type;
	COLORS color;

public:
	Card();
	Card(CARDS _type, COLORS _color);
	CARDS getType() const;
	COLORS getColor() const;
	bool legalCard(Card other) const;
	string to_string() const;
};